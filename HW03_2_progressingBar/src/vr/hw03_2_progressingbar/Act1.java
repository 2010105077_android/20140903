package vr.hw03_2_progressingbar;

import android.os.*;
import android.support.v7.app.*;
import android.view.*;
import android.widget.*;


public class Act1 extends ActionBarActivity {
	private static final int PROGRESS = 0x1;
	
	private ProgressBar mProgress;
	private int mProgressStatus = 0;
	
	private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        mProgress = (ProgressBar) findViewById(R.id.progressBar1);
        
        new Thread(new Runnable() {
        	@Override
        	public void run() {
        		while (true) {
        			if (mProgressStatus < 100)
        				mProgressStatus += 10;
        			else
        				mProgressStatus = 0;
        			
        			mHandler.post(new Runnable() {
        				@Override
        				public void run() {
        					mProgress.setProgress(mProgressStatus);
        				}
        			});
        			try {
        				Thread.sleep(300);
        			} catch (InterruptedException e) {
        				e.printStackTrace();
        			}
        		}
        		
        		
        	}
        }).start();
 }
        		
        		
        	
        
    


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.act1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
