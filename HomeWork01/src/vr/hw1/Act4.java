package vr.hw1;

import android.app.*;
import android.content.*;
import android.os.Bundle;
import android.util.*;
import android.view.*;
import android.view.View.*;
import android.widget.*;

public class Act4 extends Activity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.i("Act4", "onCreate");
	    super.onCreate(savedInstanceState);
	
	    // TODO Auto-generated method stub
	    setContentView(R.layout.act4);
	    
	    /* 버튼의 인스턴스 확인 */
		Button go_act3 = (Button) findViewById(R.id.btn_act4_go3);
		/* 이벤트를 받기 위한 리스너 작성 */
		go_act3.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Log.i("Act4.btn_act4_go3", "onClick");

				/* 인텐트를 생성한 후 명시적 다음 액티비티를 호출 */
				Intent intent1 = new Intent(
						Act4.this, Act3.class);
				startActivity(intent1);
			}
		});
	}
	
	@Override
	public void onStart() {
		Log.i("Act4", "onStart");
		super.onStart();
	}

	@Override
	public void onPause() {
		Log.i("Act4", "onPause");
		super.onPause();
	}

	@Override
	public void onStop() {
		Log.i("Act4", "onStop");
		super.onStop();
	}

	@Override
	public void onResume() {
		Log.i("Act4", "onResume");
		super.onResume();
	}

	@Override
	public void onRestart() {
		Log.i("Act4", "onRestart");
		super.onRestart();
	}

	@Override
	public void onDestroy() {
		Log.i("Act4", "onDestroy");
		super.onDestroy();
	}

}
