package vr.hw1;

import android.app.*;
import android.content.*;
import android.os.Bundle;
import android.util.*;
import android.view.*;
import android.view.View.*;
import android.widget.*;

public class Act2 extends Activity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.i("Act2", "onCreate");
	    super.onCreate(savedInstanceState);
	
	    // TODO Auto-generated method stub
	    setContentView(R.layout.act2);
	    
	    /* 버튼의 인스턴스 확인 */
		Button go_act1 = (Button) findViewById(R.id.btn_act2_go1);
		/* 이벤트를 받기 위한 리스너 작성 */
		go_act1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Log.i("Act2.btn_act2_go1", "onClick");

				/* 인텐트를 생성한 후 명시적 다음 액티비티를 호출 */
				Intent intent1 = new Intent(
						Act2.this, Act1.class);
				startActivity(intent1);
			}
		});
		
		 /* 버튼의 인스턴스 확인 */
		Button go_act3 = (Button) findViewById(R.id.btn_act2_go3);
		/* 이벤트를 받기 위한 리스너 작성 */
		go_act3.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Log.i("Act2.btn_act2_go3", "onClick");

				/* 인텐트를 생성한 후 명시적 다음 액티비티를 호출 */
				Intent intent1 = new Intent(
						Act2.this, Act3.class);
				startActivity(intent1);
			}
		});
		
		 /* 버튼의 인스턴스 확인 */
		Button go_act4 = (Button) findViewById(R.id.btn_act2_go4);
		/* 이벤트를 받기 위한 리스너 작성 */
		go_act4.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Log.i("Act2.btn_act2_go4", "onClick");

				/* 인텐트를 생성한 후 명시적 다음 액티비티를 호출 */
				Intent intent1 = new Intent(
						Act2.this, Act4.class);
				startActivity(intent1);
			}
		});
	}
	
	@Override
	public void onStart() {
		Log.i("Act2", "onStart");
		super.onStart();
	}

	@Override
	public void onPause() {
		Log.i("Act2", "onPause");
		super.onPause();
	}

	@Override
	public void onStop() {
		Log.i("Act2", "onStop");
		super.onStop();
	}

	@Override
	public void onResume() {
		Log.i("Act2", "onResume");
		super.onResume();
	}

	@Override
	public void onRestart() {
		Log.i("Act2", "onRestart");
		super.onRestart();
	}

	@Override
	public void onDestroy() {
		Log.i("Act2", "onDestroy");
		super.onDestroy();
	}

}
