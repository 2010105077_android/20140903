package vr.hw1;

import android.app.*;
import android.content.*;
import android.os.Bundle;
import android.util.*;
import android.view.*;
import android.view.View.*;
import android.widget.*;

public class Act3 extends Activity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.i("Act3", "onCreate");
	    super.onCreate(savedInstanceState);
	
	    // TODO Auto-generated method stub
	    setContentView(R.layout.act3);
	    
	    /* 버튼의 인스턴스 확인 */
		Button go_act1 = (Button) findViewById(R.id.btn_act3_go1);
		/* 이벤트를 받기 위한 리스너 작성 */
		go_act1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Log.i("Act3.btn_act3_go1", "onClick");

				/* 인텐트를 생성한 후 명시적 다음 액티비티를 호출 */
				Intent intent1 = new Intent(
						Act3.this, Act1.class);
				startActivity(intent1);
			}
		});
	}
	
	@Override
	public void onStart() {
		Log.i("Act3", "onStart");
		super.onStart();
	}

	@Override
	public void onPause() {
		Log.i("Act3", "onPause");
		super.onPause();
	}

	@Override
	public void onStop() {
		Log.i("Act3", "onStop");
		super.onStop();
	}

	@Override
	public void onResume() {
		Log.i("Act3", "onResume");
		super.onResume();
	}

	@Override
	public void onRestart() {
		Log.i("Act3", "onRestart");
		super.onRestart();
	}

	@Override
	public void onDestroy() {
		Log.i("Act3", "onDestroy");
		super.onDestroy();
	}

}
