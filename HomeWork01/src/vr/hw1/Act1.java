package vr.hw1;

import android.content.*;
import android.os.*;
import android.support.v7.app.*;
import android.util.*;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.*;


public class Act1 extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	Log.i("Act1", "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act1);
        
		/* 버튼의 인스턴스 확인 */
		Button go_act2 = (Button) findViewById(R.id.btn_act1_go2);
		/* 이벤트를 받기 위한 리스너 작성 */
		go_act2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Log.i("Act1.btn_act1_go2", "onClick");

				/* 인텐트를 생성한 후 명시적 다음 액티비티를 호출 */
				Intent intent1 = new Intent(
						Act1.this, Act2.class);
				startActivity(intent1);
			}
		});
		
		/* 버튼의 인스턴스 확인 */
		Button go_act3 = (Button) findViewById(R.id.btn_act1_go3);
		/* 이벤트를 받기 위한 리스너 작성 */
		go_act3.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Log.i("Act1.btn_act1_go3", "onClick");

				/* 인텐트를 생성한 후 명시적 다음 액티비티를 호출 */
				Intent intent1 = new Intent(
						Act1.this, Act3.class);
				startActivity(intent1);
			}
		});

	}

	@Override
	public void onStart() {
		Log.i("Act1", "onStart");
		super.onStart();
	}

	@Override
	public void onPause() {
		Log.i("Act1", "onPause");
		super.onPause();
	}

	@Override
	public void onStop() {
		Log.i("Act1", "onStop");
		super.onStop();
	}

	@Override
	public void onResume() {
		Log.i("Act1", "onResume");
		super.onResume();
	}

	@Override
	public void onRestart() {
		Log.i("Act1", "onRestart");
		super.onRestart();
	}

	@Override
	public void onDestroy() {
		Log.i("Act1", "onDestroy");
		super.onDestroy();
	}

		


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.act1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
